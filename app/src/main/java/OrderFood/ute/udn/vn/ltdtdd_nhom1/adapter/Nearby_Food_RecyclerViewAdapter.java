package OrderFood.ute.udn.vn.ltdtdd_nhom1.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import OrderFood.ute.udn.vn.ltdtdd_nhom1.R;
import OrderFood.ute.udn.vn.ltdtdd_nhom1.activity.HomeActivity;
import OrderFood.ute.udn.vn.ltdtdd_nhom1.model.Nearby_Food;

public class Nearby_Food_RecyclerViewAdapter extends RecyclerView.Adapter<Nearby_Food_RecyclerViewAdapter.Fragment_NearbyViewHolder> {
     Context context;
   List<Nearby_Food> nearbyFood;

    public Nearby_Food_RecyclerViewAdapter(Context context, List<Nearby_Food> nearbyFood) {
        this.context = context;
        this.nearbyFood = nearbyFood;
    }

    @NonNull
    @Override
    public Fragment_NearbyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.nearby_food_iteam, parent, false);
        return new Nearby_Food_RecyclerViewAdapter.Fragment_NearbyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Fragment_NearbyViewHolder holder, int position) {
        holder.imgStore.setImageResource(nearbyFood.get(position).getImageStore());
        holder.txtStore.setText(nearbyFood.get(position).getNameStore());
        holder.txtLocation.setText(nearbyFood.get(position).getLocation());
        holder.txtReviewers.setText(nearbyFood.get(position).getReviewers());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, HomeActivity.class);
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return nearbyFood.size();
    }

    public static final class Fragment_NearbyViewHolder extends RecyclerView.ViewHolder{
        ImageView imgStore;
        TextView txtStore, txtLocation, txtRating, txtTime, txtDistance, txtReviewers;

        public Fragment_NearbyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgStore = itemView.findViewById(R.id.imageStore);
            txtStore = itemView.findViewById(R.id.txtNameStore);
            txtDistance = itemView.findViewById(R.id.txtDistance);
            txtLocation = itemView.findViewById(R.id.txtLocation);
            txtRating = itemView.findViewById(R.id.txtRating);
            txtTime = itemView.findViewById(R.id.txtTime);
            txtReviewers = itemView.findViewById(R.id.txtReviewers);
        }
    }
}
