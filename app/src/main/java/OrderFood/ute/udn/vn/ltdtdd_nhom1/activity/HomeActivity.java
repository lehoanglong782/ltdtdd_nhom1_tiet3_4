package OrderFood.ute.udn.vn.ltdtdd_nhom1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import OrderFood.ute.udn.vn.ltdtdd_nhom1.R;
import OrderFood.ute.udn.vn.ltdtdd_nhom1.adapter.CategoriesAdapter;
import OrderFood.ute.udn.vn.ltdtdd_nhom1.adapter.SaleFoodAdapter;
import OrderFood.ute.udn.vn.ltdtdd_nhom1.model.Categories;
import OrderFood.ute.udn.vn.ltdtdd_nhom1.model.SaleFood;


public class HomeActivity extends AppCompatActivity {
    RecyclerView categoryCycle, saleFoodCycle;
    SaleFoodAdapter saleFoodAdapter;
    CategoriesAdapter categoriesAdapter;
    Spinner spinnerTinhThanh;
    ArrayList<String> arrayList;
    TextView tv_select;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        tv_select = findViewById(R.id.selection);
        spinnerTinhThanh = findViewById(R.id.spChiNhanh);
        tabLayout = findViewById(R.id.tablayout);

        tabLayout.addTab(tabLayout.newTab().setText("Gần tôi"));
        tabLayout.addTab(tabLayout.newTab().setText("Bán chạy"));
        tabLayout.addTab(tabLayout.newTab().setText("Đánh giá"));
        tabLayout.addTab(tabLayout.newTab().setText("Giao nhanh"));

        spinnerTinhThanhAdd(); //Spinn chon tinh thanh
        sliderImage(); //Slide banner
        categoryAdd(); //Category muc chon
        hotsaleAdd(); //Them cac mon khuyen mai

    }

    private void hotsaleAdd() {
        List<SaleFood> saleFoods = new ArrayList<>();
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        saleFoods.add(new SaleFood("Bánh tráng cô Tiên", "THCS Kim Đồng", R.drawable.fried_chicken));
        setSaleRecycler(saleFoods);
    }

    private void setSaleRecycler(List<SaleFood> saleFoods) {
        saleFoodCycle = findViewById(R.id.hot_sale_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        saleFoodCycle.setLayoutManager(layoutManager);
        saleFoodAdapter = new SaleFoodAdapter(this, saleFoods);
        saleFoodCycle.setAdapter(saleFoodAdapter);
    }

    private void spinnerTinhThanhAdd() {
        arrayList = new ArrayList<>();
        arrayList.add("Hà Nội");
        arrayList.add("Đà Nẵng");
        arrayList.add("TP.HCM");

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
        spinnerTinhThanh.setAdapter(arrayAdapter);
        //Bắt sự kiện cho Spinner, khi chọn phần tử nào thì hiển thị lên Toast
        spinnerTinhThanh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //đối số postion là vị trí phần tử trong list Data
                tv_select.setText(arrayList.get(position));
                Toast.makeText(HomeActivity.this, arrayList.get(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(HomeActivity.this, "onNothingSelected", Toast.LENGTH_SHORT).show();
            }
        }); //

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        return true;
                    case R.id.order:
                        startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.favourite:
                        startActivity(new Intent(getApplicationContext(), FavouriteActivity.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.profile:
                        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                        overridePendingTransition(0, 0);
                        return true;
                    case R.id.nearby:
                        startActivity(new Intent(getApplicationContext(), NearbyActivity.class));
                        overridePendingTransition(0, 0);
                        return true;
                }
                return false;
            }
        });
    }

    private void categoryAdd() {
        List<Categories> categories = new ArrayList<>();
        categories.add(new Categories("Trà Sữa", R.drawable.ic_all));
        categories.add(new Categories("Cơm", R.drawable.cafe));
        categories.add(new Categories("Ăn Vặt", R.drawable.milk_tea));
        categories.add(new Categories("Hoa", R.drawable.drink));
        categories.add(new Categories("Đặt Bàn", R.drawable.hamburger));
        categories.add(new Categories("Thú Cưng", R.drawable.cake));
        categories.add(new Categories("Thuốc", R.drawable.fried_chicken));
        categories.add(new Categories("Siêu Thị", R.drawable.fried_chicken));
        categories.add(new Categories("Đặt Bàn", R.drawable.hamburger));
        categories.add(new Categories("Thú Cưng", R.drawable.cake));
        categories.add(new Categories("Thuốc", R.drawable.fried_chicken));
        categories.add(new Categories("Siêu Thị", R.drawable.fried_chicken));
        setCategoriesRecycle(categories);
    }

    private void sliderImage() {
        ImageSlider imageSlider = findViewById(R.id.imageSlider);
        List<SlideModel> slideModels = new ArrayList<>();
        slideModels.add(new SlideModel(R.drawable.sale_banner, ScaleTypes.FIT));
        slideModels.add(new SlideModel(R.drawable.sale_banner1, ScaleTypes.FIT));
        slideModels.add(new SlideModel(R.drawable.sale_banner2, ScaleTypes.FIT));
        slideModels.add(new SlideModel(R.drawable.sale_banner3, ScaleTypes.FIT));
        slideModels.add(new SlideModel(R.drawable.sale_banner4, ScaleTypes.FIT));
        imageSlider.setImageList(slideModels, ScaleTypes.CENTER_INSIDE);
    }


    private void setCategoriesRecycle(List<Categories> categoriesList) {
        categoryCycle = findViewById(R.id.categories_recycle);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        categoryCycle.setLayoutManager(layoutManager);
        categoriesAdapter = new CategoriesAdapter(this, categoriesList);
        categoryCycle.setAdapter(categoriesAdapter);
    }


}